import nunjucks from 'nunjucks';

class TemplateEngine {
    #nunjucks;

    /**
     * (class description goes here)
     * @param {string} rootDir path to a directory; used to resolve file paths
     */
    constructor(rootDir) {
        const fs = new nunjucks.FileSystemLoader(rootDir)
        const env = new nunjucks.Environment(fs)
        this.#nunjucks = env;
    }

    /**
     * Render a nunjucks string
     * @param {string} content string to render
     * @param {object} [context] variables to add to the string
     * @returns {string} the transformed string
     */
    render(content, context={}) {
        return this.#nunjucks.renderString(content, context);
    }

    get nunjucks() {
        return this.#nunjucks;
    }
}

export default TemplateEngine;
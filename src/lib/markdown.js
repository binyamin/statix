import { unified } from "unified";
import parse from "remark-parse";
import gfm from "remark-gfm";
import remarkRehype from "remark-rehype";
import raw from "rehype-raw";
import html from "rehype-stringify";

export default async function(chunk) {
    const vFile = await unified()
        .use(parse)
        .use(gfm)
        .use(remarkRehype, {allowDangerousHtml: true})
        .use(raw)
        .use(html)
        .process(String(chunk));
    return vFile.toString();
};
import path from 'node:path';
import {access, readFile} from 'node:fs/promises';

import graymatter from 'gray-matter';

import markdown from './lib/markdown.js';
import TemplateEngine from './lib/templateEngine.js';
import { outputFile, walk } from './common/fs.js';

async function main(from, to, {layouts}) {
    const all = await walk(from);
    const e = new TemplateEngine(layouts);

    for(const filepath of all) {
        const rawContent = await readFile(filepath, 'utf-8');
        const { content, data } = graymatter(rawContent);

        let html = await markdown(content);

        const layout = data.template;
        if(layout) html = `{% extends '${layout}' %}{% block content %}${html}{% endblock content %}`;

        const compiled = e.render(html, data);

        const outputPath = path.join(to, path.relative(from, filepath)).replace(/\.(md|mkd|markdown)$/, '.html');
        await outputFile(outputPath, compiled, 'utf-8');
    }
}

export default main;
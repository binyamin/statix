import test from "ava";
import mock from "mock-fs";

// import after 'mock-fs'
import { access } from "node:fs/promises";
import sass from './sass.js';

test.before(async (t) => {
    mock({
        'sass': {
            "style.scss": `@use 'partials/typography'; body { color: red; }`,
            "reset.scss": "* { padding: 0; margin: 0; box-sizing: border-box; }",
            "partials": {
                "_typography.scss": "html { font-family: sans-serif; }"
            }
        }
    })
    await sass('sass', 'out/css');
})

test("single sass input should save", async (t) => {
    await access("out/css/style.css");
    t.pass();
});

test("multiple sass inputs should save", async t => {
    await access("out/css/style.css");
    await access("out/css/reset.css");
    t.pass();
});

test.cb("sass partials should not save", t => {
    access("out/css/partials/_typography.css")
    .then(r => t.fail())
    .catch(e => {
        if(e.code === "ENOENT") return t.end();
        else return t.end(e);
    })
});

test("sourcemap should exist", async t => {
    await access("out/css/style.css.map")
    await access("out/css/reset.css.map")
    t.pass();
});
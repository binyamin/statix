import path from 'node:path';

import { copyDir } from './common/fs.js';
import { inputDirs } from './common/constants.js';

import sass from './sass.js';
import templates from './template.js';
import content from './content.js';

async function assets(from, to) {
    return await copyDir(from, to);
}

export default async function build(from, to) {
    const dirs = inputDirs(from);
    
    await Promise.all([
        assets(dirs.assets, to),
        sass(dirs.sass, path.resolve(to, 'css')),
        templates(dirs.templates, to),
        content(dirs.markdown, to, { layouts: dirs.templates })
    ]);
}
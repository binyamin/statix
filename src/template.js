import path from 'node:path';
import {access, readFile} from 'node:fs/promises';

import TemplateEngine from './lib/templateEngine.js';
import { outputFile } from './common/fs.js';

const templates = [
    "index.html",
    "404.html"
]

async function main(from, to) {
    const e = new TemplateEngine(from);

    for(const t of templates) {
        const t_abs = path.join(from, t)

        try {
            await access(t_abs)
        } catch (error) {
            if(error.code === "ENOENT") continue;
            else { throw error }
        }

        const s = await readFile(t_abs, 'utf-8');

        const str = e.render(s);
        await outputFile(path.join(to, t), str, 'utf-8');
    }
}

export default main;
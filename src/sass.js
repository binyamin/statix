import { promisify } from 'node:util';
import path from 'node:path';

import sass from 'sass';

import { outputFile, walk } from './common/fs.js';

async function main(from, to) {
    const output = (fname) => {
        return path.join(to, path.relative(from, fname)).replace(/\.scss$/, '.css')
    };

    const all = await walk(from);
    const inputs = all.filter(fn => path.basename(fn)[0] !== '_');
    for(const filename of inputs) {
        const res = await promisify(sass.render)({
            file: filename,
            outFile: output(filename),
            sourceMap: true,
            // includePaths: ['node_modules']
        })
        await outputFile(output(filename), res.css, 'utf-8');
        if(res.map) await outputFile(output(filename) + ".map", res.map, 'utf-8');
    }
}

export default main;
import { inspect } from "node:util";
import * as colors from "yoctocolors";

import Timer from './timer.js'

const log = {
    _timers: {},
    enabled: true,
    log(...data) {
        if(this.enabled) {
            console.log(...data)
        }
    },
    debug(...data) {
        if(this.enabled) {
            console.error(...data.map(d => inspect(d, { colors: true })))
        }
    },
    info(...data){
        this.log(colors.bold(...data))
    },
    time(label="default") {
        const t = new Timer();
        this._timers[label] = t;
        t.start();
    },
    timeEnd(label="default") {
        this._timers[label].end();
        const ms = (this._timers[label].diff/1e6).toFixed(1);
        delete this._timers[label];
        this.log(colors.bold(colors.green(`Done in ${ms}ms`)))
    }
};

export default log;
class Timer {
    #t_init = -1;
    #t_diff = -1;

    start() {
        this.#t_init = process.hrtime();
    }
    
    end() {
        this.#t_diff = process.hrtime(this.#t_init);

        if(this.#t_init === -1) {
            this.#t_diff = -1;
            throw new Error("Must call `start()` before `end()`")
        }
    }
    
    /**
     * @type {number} diff in nanoseconds (ns)
     */
    get diff() {
        if(this.#t_diff === -1) {
            throw new Error("Must call `end()` before getting `diff`")
        }
        const NS_PER_SEC = 1e9;
        return (this.#t_diff[0] * NS_PER_SEC) + this.#t_diff[1];
    }
}

export default Timer;
import path from "node:path";

// TODO update tests to use this function
export function inputDirs(root) {
    return {
        templates: path.resolve(root, 'templates'),
        sass: path.resolve(root, 'sass'),
        markdown: path.resolve(root, 'content'),
        assets: path.resolve(root, 'static')
    }
}

// TODO add function for `DEST` file-path
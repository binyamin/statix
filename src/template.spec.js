import test from "ava";
import mock from "mock-fs";

// import after 'mock-fs'
import { access, readFile } from "node:fs/promises";

import tpl from './template.js';

test.before(async t => {
    mock({
        'templates': {
            "index.html": "{% block content %}Home page{%endblock content%}",
            "404.html": "{% extends 'index.html' %}{% block content %}404 Not found{%endblock content%}",
            "page.html": "{% extends 'index.html' %}{%block content%}Random page{%endblock content%}"
        }
    })
    await tpl('templates', 'out');
})

test("renders `index.html` template", async t => {
    await access("out/index.html")
    const o = await readFile("out/index.html", 'utf-8');
    t.is(o, 'Home page')
})

test("renders `404.html` template", async t => {
    await access("out/404.html")
    const o = await readFile("out/404.html", 'utf-8');
    t.is(o, '404 Not found')
})

test("Does not render `page.html`", async t => {
    await t.throwsAsync(access("out/page.html"))
})
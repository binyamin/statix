import test from "ava";

import mock from "mock-fs";

// import after 'mock-fs'
import { readFile } from "node:fs/promises";

import content from './content.js';

test.before(async t => {
    mock({
        'content': {
            'about.md': "---\ntemplate: 'index.html'\n---\n# Heading One"
        },
        'templates': {
            "index.html": "<html><body>{% block content %}Home page{%endblock content%}</body></html>",
        }
    })
    await content('content', 'out', {layouts: 'templates'});
})

test("markdown with `template` front-matter should use the respective template file", async t => {
    const got = await readFile("out/about.html", 'utf-8');
    t.log(got);
    t.is(got, "<html><body><h1>Heading One</h1></body></html>");
})
# Software Architecture
templateEngine receives tpl string and transforms to html string.
sass receives sass string and transforms to css string

## Default structure for input directory
`templates/` (html) - pages, layouts, partials
`sass/` (scss) - sass
`static/` (*) - assets; copied directly to the build root
`content/` (md) - markdown content for dynamically generated pages

## Program Flow
- static
    1. copy
- css
    1. walk
    2. filter (remove files with leading underscore)
    3. sass
    4. output
- html
    1. read
    2. template
    3. output
- content
    1. walk
    2. read
    3. template
    4. remark
    5. output
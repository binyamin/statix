":" //#;exec /usr/bin/env node --input-type=module - "$@" < "$0"
import {program} from 'commander';
import del from 'del';

import log from './src/common/log.js';
import build from './src/build.js';

program
    .name("statix")
    .version("0.1.0--pre.1", '-v, --version')
    .description("A squeezable binary named George")

program.command("build")
    .description("Deletes the output directory, if there is one, and builds the site")
    .option("-o, --output <dir>", "Output the generated site in the given path", "out")
    .option("-q, --quiet", "display less output", false) // TODO move to global option
    .action(async ({output, quiet}) => {
        if(quiet) log.enabled = false;
        await del(output);
        log.info("Building site...");
        log.time("build");
        await build('.', output); // TODO log build stats (eg. number of HTML pages)
        log.timeEnd("build");
    });

program.command("serve")
    .description("Serve the site. Rebuild and reload on change automatically")
    .option("-p, --port <port>", "Which port to use", 3000)
    .action(({port}) => {
        log.info(`Serving on localhost:${port}`);
    });

program.parse(process.argv);